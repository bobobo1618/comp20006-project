/**********************************************************
*                                                         *
*  COMP20006 Project 1                                    *
*                                                         *
*  Name: Disassembler.c                                   *
*  Description: The main file for this MIPS disassembler. *
*                                                         *
*  HIDE_START                                             *
*  Author: Lucas Cooper, 540969                           *
*  HIDE_END                                               *
*                                                         *
**********************************************************/

// Include the header for all the important stuff.
#include "res.h"

// Maximum size of input file.
#define MAXSIZE 1000

// The function that essentially just hands off the work to the functions in res.c
void process(unsigned int code){
    // Break up the instruction using bitshifting. Fun! If you're wondering what all these actually are,
    // I got most of the information I used from here: http://www.d.umn.edu/~gshute/spimsal/talref.html
    // and I'm using their terminology.

    unsigned int opcode = (code >> 26);
    unsigned int target; // Target address for jump instructions.

    // R-Type stuff. It's only used for mul so only calculated then.
    unsigned int rs, rt, rd;

    if(code != 0){
        // Check for coprocessor instructions. If there isn't one, switch over to processing.
        if(opcode > 0x010000){
            puts("Found coprocessor instruction.");
        } else {
	        // Switch statements are horrendously overused in this project. If you don't know what they are,
	        // basically it compares the value in the brackets to the values for each case and executes the
	        // code after it. (This is for code review people, not staff :P )
	        switch(opcode){
	        	// First check for boring things...
	            case 0:
	                rtype(code);
	                break;
	            case 0x2:
	                // Get the target of the jump and print it.
	                target = 4*((code << 6) >> 6);

	                // You'll see this format around a bit. Basically, it makes sure the output is 10 
	                // characters long, hexadecimal and has the 0x on the front.
	                printf("j %#010x", target); 
	                break;
	            case 0x3:
	                target = 4*((code << 6) >> 6);
	                printf("jal %#010x", target);
	                break;
	            case 0x1c:
	                // Apparently this is mul? I can't seem to find it in any of the online MIPS references 
	                // and it's annoying me.

	            	// Break up the instruction like an R-Type (because that's what it is. It just has a dumb
	            	// opcode).

	            	rs = ((code << 6) >> 27);
    				rt = ((code << 11) >> 27);
    				rd = ((code << 16) >> 27);

    				// Print the instruction.
	                printf("mul $%d, $%d, $%d", rd, rs, rt);
	                break;
	            default:
	            	// If it's not one of the above opcodes, it's probably an I-Type. Send it over there.
	                itype(code);
	                break;
	        }
	        // I'll always need a newline so I may as well print it down here.
	        printf("\n");
    	}
    } else {
    	// YAY NOP
        puts("nop");
    }
}

int main(int argc, char **argv){
	// Make an array for codes. I don't actually use it but it could be useful!
    unsigned int machinecode[MAXSIZE];

    // Redirects stdout to a file. In case I decide to actually do stage C.
    // freopen("output.txt", "w", stdout);

    // Declare the variables...
    unsigned int count;
    unsigned int currentcode;

    // Use them!
    for (count = 0; count <= MAXSIZE; count++){
        // Scan for a hexadecimal input value.
        scanf("%x", &currentcode);

        // Add it to the array.
        machinecode[count] = currentcode;


        // Check to make sure the input isn't too long.
        if(count == MAXSIZE){
            puts("Looks like the input limit has been reached. That ain't good.");
        }

        // Wait for end of stream.
        if(currentcode == 0xffffffff){
            break;
        } else {
            // Print address and instruction like SPIM does. An explanation of the format is up there ^^^
            // somewhere.
            printf("[%#010x] %#010x ", 0x00400000 + (count*4), currentcode);
            process(currentcode);
        }
    }
}