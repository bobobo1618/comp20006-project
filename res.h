/**********************************************************
*                                                         *
*  COMP20006 Project 1                                    *
*                                                         *
*  Name: res.h                                            *
*  Description: The header file for all the actual work.  *
*                                                         *
*  HIDE_START                                             *
*  Author: Lucas Cooper, 540969                           *
*  HIDE_END                                               *
*                                                         *
**********************************************************/

#include <stdio.h>

// Function declarations. See res.c for actual implementation.
void itypeargs(unsigned int opcode, unsigned int rs, unsigned int rt, unsigned int immediate);
void rtypeargs(unsigned int opcode, unsigned int rs, unsigned int rt, unsigned int rd, unsigned int sa, unsigned int function);

void itype(unsigned int code);
void rtype(unsigned int code);