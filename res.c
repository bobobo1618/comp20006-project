/**********************************************************
*                                                         *
*  COMP20006 Project 1                                    *
*                                                         *
*  Name: res.c                                            *
*  Description: The actual work...                        *
*                                                         *
*  HIDE_START                                             *
*  Author: Lucas Cooper, 540969                           *
*  HIDE_END                                               *
*                                                         *
**********************************************************/

#include "res.h"

void itype(unsigned int code){
    /* I couldn't be bothered doing a loop so I made Python write a great big switch statement for me 
    * using input from http://www.d.umn.edu/~gshute/spimsal/talref.html
    *
    * That's programs writing programs ;) Soon I'll have AI...
    *
    * Oh, and in case you were wondering what I'd have done if this wasn't quicker, I'd make an array of structs
    * containing the opcode and instruction for each instruction for each of the I and R type instruction sets.
    * Then it would be a simple matter of storing the length of the array and doing a for loop when I want to 
    * determine the instruction. But as I said, this was quicker, if more convoluted. Besides, Sublime Text and 
    * Vim both have code folding built in so this isn't that much trouble.
	* 
	* Also, I'm using the entire MIPS instruction set because it's easier to let Python do the entire lot for me 
	* than it is for me to partially do it by hand.
    */

    unsigned int opcode = code >> 26;
    unsigned int rs = ((code << 6) >> 27);
    unsigned int rt = ((code << 11) >> 27);
    unsigned int immediate = ((code << 16) >> 16);

    //Let the switching begin...
    switch(opcode){
        case 0x8:
            printf("addi ");
            break;

        case 0x9:
            printf("addiu ");
            break;

        case 0xc:
            printf("andi ");
            break;

        case 0x4:
            printf("beq ");
            break;

        case 0x5:
            printf("bne ");
            break;

        case 0x20:
            printf("lb ");
            break;

        case 0x24:
            printf("lbu ");
            break;

        case 0x21:
            printf("lh ");
            break;

        case 0x25:
            printf("lhu ");
            break;

        case 0xf:
            printf("lui ");
            break;

        case 0x23:
            printf("lw ");
            break;

        case 0x31:
            printf("lwc1 ");
            break;

        case 0xd:
            printf("ori ");
            break;

        case 0x28:
            printf("sb ");
            break;

        case 0xa:
            printf("slti ");
            break;

        case 0xb:
            printf("sltiu ");
            break;

        case 0x29:
            printf("sh ");
            break;

        case 0x2b:
            printf("sw ");
            break;

        case 0x39:
            printf("swc1 ");
            break;

        case 0xe:
            printf("xori ");
            break;

        // Handle silly weird instructions.
        case 0x6:
            if(rt == 0){
                printf("blez ");
            }
            break;

        case 0x7:
            if(rt == 0){
                printf("bgtz ");
            }
            break;

        // These two share an opcode but are weird and use rt.
        case 0x1:
            if(rt == 1){
                printf("bgez ");
            } else if (rt == 0){
                printf("bltz ");
            } else {
                printf("Bad 0x0000001 opcode. ");
            }
        default:
            printf("Unknown I-Type instruction found: %#010x", opcode); // Yay error handling!
    }

    // Print the arguments to the function.
    itypeargs(opcode, rs, rt, immediate);
}

// Prints the arguments to an I-Type (immediate) instruction.
void itypeargs(unsigned int opcode, unsigned int rs, unsigned int rt, unsigned int immediate){
	/******************************************************** 
	*
	* A variable that basically stores which format to print.
	*
    *    1: $rt, $rs, immediate
    *    2: $rs, $rt, immediate
    *    3: $rs, immediate
    *    4: $rt, $immediate(rs)
    *    5: $rt, immediate
    *
    *********************************************************/
            
    int whatdo;

    // Get format of output and store it. If you're wondering which opcode is which, look above. This is
    // basically copied from there anyway.
    switch(opcode){
        case 0x8:
            whatdo = 1;
            break;

        case 0x9:
            whatdo = 1;
            break;

        case 0xc:
            whatdo = 1;
            break;

        case 0x4:
            whatdo = 2;
            break;

        case 0x5:
            whatdo = 2;
            break;

        case 0x20:
            whatdo = 4;
            break;

        case 0x24:
            whatdo = 4;
            break;

        case 0x21:
            whatdo = 4;
            break;

        case 0x25:
            whatdo = 4;
            break;

        case 0xf:
            whatdo = 5;
            break;

        case 0x23:
            whatdo = 4;
            break;

        case 0x31:
            whatdo = 4;
            break;

        case 0xd:
            whatdo = 1;
            break;

        case 0x28:
            whatdo = 4;
            break;

        case 0xa:
            whatdo = 1;
            break;

        case 0xb:
            whatdo = 1;
            break;

        case 0x29:
            whatdo = 4;
            break;

        case 0x2b:
            whatdo = 4;
            break;

        case 0x39:
            whatdo = 4;
            break;

        case 0xe:
            whatdo = 1;
            break;

        case 0x6:
            if(rt == 0){
                whatdo = 3;
            }
            break;

        case 0x7:
            if(rt == 0){
                whatdo = 3;
            }
            break;

        case 0x1:
            if(rt == 1){
                whatdo = 3;
            } else if (rt == 0){
                whatdo = 3;
            } else {
                printf("Bad 0x0000001 opcode. ");
            }
            break;

        default:
            puts("SOMETHING BROKE!!!");
    }

    // Print stuff using the given format.
    switch(whatdo){
        case 1:
            printf("$%d, $%d, %d", rt, rs, immediate);
            break;
        case 2:
            printf("$%d, $%d, %d", rs, rt, immediate);
            break;
        case 3:
            printf("$%d, %d", rs, immediate);
            break;
        case 4:
            printf("$%d, $%d(%d)", rt, immediate, rs);
            break;
        case 5:    
            printf("$%d, %d", rt, immediate);
            break;
    }
}


// This prints R-Type (stores stuff in registers) instructions.
void rtype(unsigned int code){

	// Break stuff up! See the URL I've been throwing around to find out what they are. Or just read the 
	// code where they're used/printed.
    unsigned int opcode = code >> 26;
    unsigned int function = ((code << 26) >> 26);
    unsigned int rs = ((code << 6) >> 27);
    unsigned int rt = ((code << 11) >> 27);
    unsigned int rd = ((code << 16) >> 27);
    unsigned int sa = ((code << 21) >> 27);

    // Same deal as with the I-Types above.
    switch(function){
        case 0x20:
            printf("add ");
            break;

        case 0x21:
            printf("addu ");
            break;

        case 0x24:
            printf("and ");
            break;

        case 0xd:
            printf("break ");
            break;

        case 0x1a:
            printf("div ");
            break;

        case 0x1b:
            printf("divu ");
            break;

        case 0x9:
            printf("jalr ");
            break;

        case 0x8:
            printf("jr ");
            break;

        case 0x10:
            printf("mfhi ");
            break;

        case 0x12:
            printf("mflo ");
            break;

        case 0x11:
            printf("mthi ");
            break;

        case 0x13:
            printf("mtlo ");
            break;

        case 0x18:
            printf("mult ");
            break;

        case 0x19:
            printf("multu ");
            break;

        case 0x27:
            printf("nor ");
            break;

        case 0x25:
            printf("or ");
            break;

        case 0x0:
            printf("sll ");
            break;

        case 0x4:
            printf("sllv ");
            break;

        case 0x2a:
            printf("slt ");
            break;

        case 0x2b:
            printf("sltu ");
            break;

        case 0x3:
            printf("sra ");
            break;

        case 0x7:
            printf("srav ");
            break;

        case 0x2:
            printf("srl ");
            break;

        case 0x6:
            printf("srlv ");
            break;

        case 0x22:
            printf("sub ");
            break;

        case 0x23:
            printf("subu ");
            break;

        case 0xc:
            printf("syscall ");
            break;

        case 0x26:
            printf("xor ");
            break;
        default:
            printf("Unknown R-Type instruction found.\n");
    }

    rtypeargs(opcode, rs, rt, rd, sa, function);
}

// Same deal as with itypeargs above but with R-Type instructions.
void rtypeargs(unsigned int opcode, unsigned int rs, unsigned int rt, unsigned int rd, unsigned int sa, unsigned int function){
    int whatdo;
    /***************************
    * Formats:                 * 
    *                          *
	*	1: $rd, $rs, $rt       * 
	*	2: $rs, $rt            *
	*	3: $rd, $rs            *
	*	4: $rs                 *
	*	5: $rd                 *
	*	6: $rd, $rt, $sa       *
	*	7: $rd, $rt, $rs       *
	*                          *
	***************************/

	// Look at the same thing in itypeargs if you're wondering what this is.
    switch(function){
        case 0x20:
            whatdo = 1;
            break;

        case 0x21:
            whatdo = 1;
            break;

        case 0x24:
            whatdo = 1;
            break;

        case 0x1a:
            whatdo = 2;
            break;

        case 0x1b:
            whatdo = 2;
            break;

        case 0x9:
            whatdo = 3;
            break;

        case 0x8:
            whatdo = 4;
            break;

        case 0x10:
            whatdo = 5;
            break;

        case 0x12:
            whatdo = 5;
            break;

        case 0x11:
            whatdo = 4;
            break;

        case 0x13:
            whatdo = 4;
            break;

        case 0x18:
            whatdo = 2;
            break;

        case 0x19:
            whatdo = 2;
            break;

        case 0x27:
            whatdo = 1;
            break;

        case 0x25:
            whatdo = 1;
            break;

        case 0x0:
            whatdo = 6;
            break;

        case 0x4:
            whatdo = 7;
            break;

        case 0x2a:
            whatdo = 1;
            break;

        case 0x2b:
            whatdo = 1;
            break;

        case 0x3:
            whatdo = 6;
            break;

        case 0x7:
            whatdo = 7;
            break;

        case 0x2:
            whatdo = 6;
            break;

        case 0x6:
            whatdo = 7;
            break;

        case 0x22:
            whatdo = 1;
            break;

        case 0x23:
            whatdo = 1;
            break;

        case 0x26:
            whatdo = 1;
            break;
    }

    // Print stuff.
    switch(whatdo){
        case 1:
            printf("$%d, $%d, $%d", rd, rs, rt);
            break;
        case 2:
            printf("$%d, $%d", rs, rt);
            break;
        case 3:
            printf("$%d, $%d", rd, rs);
            break;
        case 4:
            printf("$%d", rs);
            break;
        case 5:
            printf("$%d", rd);
            break;
        case 6:
            printf("$%d, $%d, $%d", rd, rt, sa);
            break;
        case 7:
            printf("$%d, $%d, $%d", rd, rt, rs);
            break;
    }
}